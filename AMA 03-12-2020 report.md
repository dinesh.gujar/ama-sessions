# AMA Session 
***
### NaN in JS? How do you get it? 

The NaN property represents "Not-a-Number" value. This property indicates that a value is not a legal number.

This is property of global object.

The NaN property is the same as the Number.Nan property.

Tip: Use the isNaN() global function to check if a value is a NaN value.

    Syntax
        Number.NaN
#### There are several ways in which NaN can happen:
Division of zero by zero.

Dividing an infinity by an infinity.

Multiplication of an infinity by a zero.

Any operation in which NaN is an operand.

Converting a non-numeric string or undefined into a number.

***
### What are ternary operators?
The conditional (ternary) operator is the only JavaScript operator that takes three operands: a condition followed by a question mark (?), then an expression to execute if the condition is truthy followed by a colon (:), and finally the expression to execute if the condition is falsy. This operator is frequently used as a shortcut for the if statement.

        function getFee(isMember) {
        return (isMember ? '$2.00' : '$10.00');
        }

        console.log(getFee(true));
        // expected output: "$2.00"

        console.log(getFee(false));
        // expected output: "$10.00"

        console.log(getFee(null));
        // expected output: "$10.00"

***
### CSS - What is Cascading?
The cascading algorithm determines how to find the value to apply for each property for each document element.

It first filters all the rules from the different sources to keep only the rules that apply to a given element. That means rules whose selector matches the given element and which are part of an appropriate media at-rule.
Then it sorts these rules according to their importance, that is, whether or not they are followed by !important, and by their origin. The cascade is in ascending order, which means that !important values from a user-defined style sheet have precedence over normal values originated from a user-agent style sheet:

**Origin	Importance**

1	user agent	normal

2	user	normal

3	author	normal

4	animations	

5	author	!important

6	user	!important

7	user agent	!important

8	transitions	

In case of equality, the specificity of a value is considered to choose one or the other.

There are three factors to consider, listed here in increasing order of importance. Later ones overrule earlier ones:

* **Source order**
* **Specificity**
* **Importance**

***
### Ways of adding CSS in HTML
    
There are three ways of inserting a style sheet:

* **External CSS**
* **Internal CSS**
* **Inline CSS**

#### External CSS
With an external style sheet, you can change the look of an entire website by changing just one file!

Each HTML page must include a reference to the external style sheet file inside the <link> element, inside the head section.
 
 **Example**
        
        <!DOCTYPE html>
        <html>
        <head>
        <link rel="stylesheet" href="mystyle.css">
        </head>
        <body>
        <h1>This is a heading</h1>
        <p>This is a paragraph.</p>

        </body>
        </html>

#### Internal CSS  


An internal style sheet may be used if one single HTML page has a unique style.

The internal style is defined inside the style element, inside the head section.

**EXAMPLE**

        <!DOCTYPE html>
        <html>
        <head>
        <style>
        body {
        background-color: linen;
        }

        h1 {
        color: maroon;
        margin-left: 40px;
        }
        </style>
        </head>
        <body>

        <h1>This is a heading</h1>
        <p>This is a paragraph.</p>

        </body>
        </html>

#### Inline CSS

An inline style may be used to apply a unique style for a single element.

To use inline styles, add the style attribute to the relevant element. The style attribute can contain any CSS property.

**Example**

    <!DOCTYPE html>
    <html>
    <body>

    <h1 style="color:blue;text-align:center;">This is a heading</h1>
    <p style="color:red;">This is a paragraph.</p>

    </body>
    </html>


***
### Access Control Specifiers in Ruby

Ruby gives you three levels of protection:

**Public methods**

 can be called by everyone - no access control is enforced. A class's instance methods (these do not belong only to one object; instead, every instance of the class can call them) are public by default; anyone can call them. The initialize method is always private.

**Protected methods**

 can be invoked only by objects of the defining class and its subclasses. Access is kept within the family. However, usage of protected is limited.

**Private methods**

 cannot be called with an explicit receiver - the receiver is always self. This means that private methods can be called only in the context of the current object; you cannot invoke another object's private methods.

 **Example**

        class ClassAccess  
        def m1          # this method is public  
        end  
        protected  
        def m2        # this method is protected  
        end  
        private  
            def m3        # this method is private  
            end  
        end  
        ca = ClassAccess.new  
        ca.m1  
        #ca.m2  
        #ca.m3  


***
### Why responsiveness in important?

Responsive design can help you solve a lot of problems for your website. It will make your site mobile-friendly, improve the way it looks on devices with both large and small screens, and increase the amount of time that visitors spend on your site. It can also help you improve your rankings in search engines.

The ultimate goal of responsive design is to avoid the unnecessary resizing, scrolling, zooming, or panning that occurs with sites that have not been optimized for different devices. It is often very difficult to navigate these sites, and it may even cost you potential customers who become frustrated with trying to figure out how to do something.

Responsive website design also replaces the previous need to design a dedicated mobile website for smartphone users. Now, instead of designing multiple websites for different screen sizes, you can design just one website that scales up or down automatically to match the device it’s being viewed on.



***
### What are attributes writer, reader and accessor
#### Attribute Accessor:

Initializing instance variable: Instance variables can be initialized using constructor. Inside the constructor, the initial value to instance variables is provided which can be used further anywhere in the program.

**Example**    

    class Demo  
    
    # constructor  
    def initialize    
        puts "Welcome to GeeksforGeeks!"   
    end   
  
    end 

#### Attribute Reader :
attribute reader is a method that “exposes” an instance variable. It makes it accessible for others. I.e. it allows you to ask for the value of an instance variable with the same name, and does nothing but return its value. Once defined others can ask this object for knowledge that otherwise would be private, and unaccessible.

**Example**

        class Person
        def initialize(name)
            @name = name
        end

        def name
            @name
        end
        end

#### Attribute Writer :
Attribute writer is a method that assigns a value to an instance variable. It simply takes values from outside variable and assign it to the instance variable.

**Example**

    class Person
    def initialize(name)
        @name = name
    end

    def name
        @name
    end

    def password=(password)
        @password = password
    end
    end
***
### What is a web API?

A Web API is an application programming interface for either a web server or a web browser. It is a web development concept, usually limited to a web application's client-side (including any web frameworks being used), and thus usually does not include web server or browser implementation details such as SAPIs or APIs unless publicly accessible by a remote web application.

A server-side web API is a programmatic interface consisting of one or more publicly exposed endpoints to a defined re quest–response message system, typically expressed in JSON or XML, which is exposed via the web—most commonly by means of an HTTP-based web server. Mashups are web applications which combine the use of multiple server-side web APIs.

 Webhooks are server-side web APIs that take input as a Uniform Resource Identifier (URI) that is designed to be used like a remote named pipe or a type of callback such that the server acts as a client to dereference the provided URI and trigger an event on another server which handles this event thus providing a type of peer-to-peer IPC.

 Web 2.0 Web APIs often use machine-based interactions such as REST and SOAP. RESTful web APIs are typically loosely based on HTTP methods to access resources via URL-encoded parameters and the use of JSON or XML to transmit data. By contrast, SOAP protocols are standardized by the W3C and mandate the use of XML as the payload format, typically over HTTP. Furthermore, SOAP-based Web APIs use XML validation to ensure structural message integrity, by leveraging the XML schemas provisioned with WSDL documents. A WSDL document accurately defines the XML messages and transport bindings of a Web service.
***
### Git: How to make 3 commits into 1? (git rebase) 

Rebasing is the process of moving or combining a sequence of commits to a new base commit. Rebasing is most useful and easily visualized in the context of a feature branching workflow. The general process can be visualized as the following:

![Git rebase Image](https://wac-cdn.atlassian.com/dam/jcr:e4a40899-636b-4988-9774-eaa8a440575b/02.svg?cdnVersion=1348)



Usage
The primary reason for rebasing is to maintain a linear project history. For example, consider a situation where the master branch has progressed since you started working on a feature branch. You want to get the latest updates to the master branch in your feature branch, but you want to keep your branch's history clean so it appears as if you've been working off the latest master branch. This gives the later benefit of a clean merge of your feature branch back into the master branch. Why do we want to maintain a "clean history"? The benefits of having a clean history become tangible when performing Git operations to investigate the introduction of a regression. A more real-world scenario would be:

*   A bug is identified in the master branch. A feature that was working successfully is now broken.
*   A developer examines the history of the master branch using git log because of the "clean history" the developer is quickly able to reason about the history of the project.
*   The developer can not identify when the bug was introduced using git log so the developer executes a git bisect.
*   Because the git history is clean, git bisect has a refined set of commits to compare when looking for the regression. The developer quickly finds the commit that introduced the bug and is able to act accordingly.
***
